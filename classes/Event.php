<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Hammdie
 * Date: 17.06.15
 * Time: 22:16
 * To change this template use File | Settings | File Templates.
 */
class Event
{
    /** @var string */
    public $gabelstaplerNr;
    /** @var string */
    public $gabelstaplerFahrerID;
    /** @var DateTime */
    public $start;
    /** @var DateTime */
    public $end;

}
