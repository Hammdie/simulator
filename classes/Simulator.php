<?php

include_once 'Event.php';
include_once 'DatabaseManagerInterface.php';
include_once 'SendEventProviderInterface.php';

/**
 * Created by JetBrains PhpStorm.
 * User: Hammdie
 * Date: 17.06.15
 * Time: 22:16
 * To change this template use File | Settings | File Templates.
 */
class Simulator
{
    /** @var SendEventProviderInterface */
    private $provider;

    /** @var DatabaseManagerInterface */
    private $dbManager;

    /**
     * @param SendEventProviderInterface $provider
     */
    public function __construct(SendEventProviderInterface $provider, DatabaseManagerInterface $dbManager) {
        $this->provider = $provider;
        $this->dbManager = $dbManager;
    }

    /**
     *
     */
    public function procEvent() {
        $event = $this->createEvent();
        $this->provider->send($event);
        $this->dbManager->save($event);
    }

    /**
     * @return Event
     */
    protected function createEvent(){
        /** @var Event $event  */
        $event = new Event();
        $event->gabelstaplerFahrerID = $this->getValidGabelstaplerFahrerID();
        $event->gabelstaplerNr       = $this->getValidGabelstaplerNr();
        $event->start = new DateTime('NOW');
        $duration = rand(10, 200);
        $event->end = $event->start->add(new DateInterval('PT'.$duration.'S'));
        return $event;
    }



    private  function getValidGabelstaplerFahrerID() {
        return 'asdf1234';
    }

    private  function getValidGabelstaplerNr(){
        return '0123456789asdf';
    }


}
