<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Hammdie
 * Date: 17.06.15
 * Time: 22:22
 * To change this template use File | Settings | File Templates.
 */
interface SendEventProviderInterface
{
    public function send(Event $event);

}
